using MarcinWork.Core.Tools.JsonComparer.Models;
using System;
using System.IO;
using Xunit;
using Xunit.Abstractions;

namespace MarcinWork.Core.Tools.JsonComparer.Tests
{
    public class UnitTest1
    {
        private readonly ITestOutputHelper output;

        public UnitTest1(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void CompareStrings1_Equals()
        {
            string json1 = File.ReadAllText("Jsons/Json1.json");
            string json2 = File.ReadAllText("Jsons/Json2.json");

            var engine = new CompareEngine<string, string>(json1, json2, new ComparisonSettings(true));
            var result = engine.Compare();

            foreach (var diff in result.Differences)
            {
                output.WriteLine($"{diff.SourceKey} -> {diff.SourceValue} : {diff.TargetValue}");
            }
        }
    }
}
