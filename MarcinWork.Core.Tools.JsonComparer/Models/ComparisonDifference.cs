﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Core.Tools.JsonComparer.Models
{
    public class ComparisonDifference
    {
        public Enums.ComparisonDifferenceType DifferenceType { get; set; }

        public string SourceKey { get; set; }
        public string SourceValue { get; set; }
        public string TargetKey { get; set; }
        public string TargetValue { get; set; }
        public KeyValuePair<string, JToken> SourcePair { get; set; }
        public KeyValuePair<string, JToken> TargetPair { get; set; }
    }
}
