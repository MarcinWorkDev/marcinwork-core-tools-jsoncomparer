﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Core.Tools.JsonComparer.Models
{
    public class ComparisonSettings
    {
        public JsonSerializerSettings SerializerSettings { get; set; }

        public ComparisonSettings(bool defaultSettings)
        {
            InitializeSerializerSettings(defaultSettings);
        }

        private void InitializeSerializerSettings(bool defaultSettings)
        {
            if (defaultSettings)
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    NullValueHandling = NullValueHandling.Ignore
                };
                SerializerSettings.Converters.Add(new StringEnumConverter());
            }
            else
            {
                SerializerSettings = new JsonSerializerSettings();
            }
        }
    }
}
