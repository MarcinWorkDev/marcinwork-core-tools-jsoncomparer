﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Core.Tools.JsonComparer.Models.Enums
{
    public enum ComparisonDifferenceType
    {
        DifferentValue,
        NotFoundInTarget,
        DifferentFormat
    }
}
