﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Core.Tools.JsonComparer.Models
{
    public class ComparisonResult
    {
        public bool Status
        {
            get
            {
                return Differences.Count == 0 ? true : false;
            }
        }

        public List<ComparisonDifference> Differences { get; }

        public ComparisonResult()
        {
            Differences = new List<ComparisonDifference>();
        }

        public void AddDifference(ComparisonDifference diff)
        {
            Differences.Add(diff);
        }

        public void AddDifferences(IEnumerable<ComparisonDifference> diffs)
        {
            Differences.AddRange(diffs);
        }
    }
}
