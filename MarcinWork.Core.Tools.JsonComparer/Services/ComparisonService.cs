﻿using MarcinWork.Core.Tools.JsonComparer.Models;
using MarcinWork.Core.Tools.JsonComparer.Models.Enums;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarcinWork.Core.Tools.JsonComparer.Services
{
    public static class ComparisonService
    {
        public static ComparisonResult CompareObjects(JObject source, JObject target)
        {
            var result = new ComparisonResult();
            result.AddDifferences(CompareObjectsDiffs(source, target));
            return result;
        }

        public static ComparisonResult CompareArrays(JArray source, JArray target)
        {
            var result = new ComparisonResult();
            result.AddDifferences(CompareArraysDiffs(source, target));
            return result;
        }

        private static List<ComparisonDifference> CompareObjectsDiffs(JObject source, JObject target)
        {
            var diffs = new List<ComparisonDifference>();

            foreach (KeyValuePair<string, JToken> sourcePair in source)
            {
                if (sourcePair.Value.Type == JTokenType.Object)
                {
                    if (target.GetValue(sourcePair.Key) == null)
                    {
                        diffs.Add(new ComparisonDifference()
                        {
                            DifferenceType = ComparisonDifferenceType.NotFoundInTarget,
                            SourceKey = sourcePair.Key,
                            SourceValue = sourcePair.Value.Value<string>(),
                            SourcePair = sourcePair,
                            TargetKey = null,
                            TargetValue = null,
                            TargetPair = new KeyValuePair<string, JToken>()
                        });
                    }
                    else if (target.GetValue(sourcePair.Key).Type != JTokenType.Object)
                    {
                        diffs.Add(new ComparisonDifference()
                        {
                            DifferenceType = ComparisonDifferenceType.DifferentFormat,
                            SourceKey = sourcePair.Key,
                            SourceValue = sourcePair.Value.Value<string>(),
                            SourcePair = sourcePair,
                            TargetKey = sourcePair.Key,
                            TargetValue = target.GetValue(sourcePair.Key).Value<string>(),
                            TargetPair = new KeyValuePair<string, JToken>(sourcePair.Key, target.GetValue(sourcePair.Key))
                        });
                    }
                    else
                    {
                        diffs.AddRange(CompareObjectsDiffs(sourcePair.Value.ToObject<JObject>(),
                            target.GetValue(sourcePair.Key).ToObject<JObject>()));
                    }
                }
                else if (sourcePair.Value.Type == JTokenType.Array)
                {
                    if (target.GetValue(sourcePair.Key) == null)
                    {
                        diffs.Add(new ComparisonDifference()
                        {
                            DifferenceType = ComparisonDifferenceType.NotFoundInTarget,
                            SourceKey = sourcePair.Key,
                            SourceValue = sourcePair.Value.Value<string>(),
                            SourcePair = sourcePair,
                            TargetKey = null,
                            TargetValue = null,
                            TargetPair = new KeyValuePair<string, JToken>()
                        });
                    }
                    else
                    {
                        diffs.AddRange(CompareArraysDiffs(sourcePair.Value.ToObject<JArray>(),
                            target.GetValue(sourcePair.Key).ToObject<JArray>(), sourcePair.Key));
                    }
                }
                else
                {
                    JToken expected = sourcePair.Value;
                    var actual = target.SelectToken(sourcePair.Key);
                    if (actual == null)
                    {
                        diffs.Add(new ComparisonDifference()
                        {
                            DifferenceType = ComparisonDifferenceType.NotFoundInTarget,
                            SourceKey = sourcePair.Key,
                            SourceValue = sourcePair.Value.Value<string>(),
                            SourcePair = sourcePair,
                            TargetKey = null,
                            TargetValue = null,
                            TargetPair = new KeyValuePair<string, JToken>()
                        });
                    }
                    else
                    {
                        if (expected.Type != actual.Type)
                        {
                            diffs.Add(new ComparisonDifference()
                            {
                                DifferenceType = ComparisonDifferenceType.DifferentFormat,
                                SourceKey = sourcePair.Key,
                                SourceValue = expected.Value<string>(),
                                SourcePair = sourcePair,
                                TargetKey = sourcePair.Key,
                                TargetValue = actual.Value<string>(),
                                TargetPair = new KeyValuePair<string, JToken>(sourcePair.Key, actual)
                            });
                        }
                        else if (!JToken.DeepEquals(expected, actual))
                        {
                            diffs.Add(new ComparisonDifference()
                            {
                                DifferenceType = ComparisonDifferenceType.DifferentValue,
                                SourceKey = sourcePair.Key,
                                SourceValue = expected.Value<string>(),
                                SourcePair = sourcePair,
                                TargetKey = sourcePair.Key,
                                TargetValue = actual.Value<string>(),
                                TargetPair = new KeyValuePair<string, JToken>(sourcePair.Key, actual)
                            });
                        }
                    }
                }
            }

            return diffs;
        }

        private static List<ComparisonDifference> CompareArraysDiffs(JArray source, JArray target, string arrayName = "")
        {
            var diffs = new List<ComparisonDifference>();

            for (var index = 0; index < source.Count; index++)
            {
                var expected = source[index];
                if (expected.Type == JTokenType.Object)
                {
                    var actual = (index >= target.Count) ? new JObject() : target[index];
                    diffs.AddRange(CompareObjectsDiffs(expected.ToObject<JObject>(),
                        actual.ToObject<JObject>()));
                }
                else
                {
                    var actual = (index >= target.Count) ? "" : target[index];
                    if (!JToken.DeepEquals(expected, actual))
                    {
                        if (String.IsNullOrEmpty(arrayName))
                        {
                            diffs.Add(new ComparisonDifference() {
                                DifferenceType = ComparisonDifferenceType.DifferentValue,
                                SourceKey = index.ToString(),
                                SourceValue = expected.Value<string>(),
                                SourcePair = new KeyValuePair<string, JToken>(index.ToString(), expected),
                                TargetKey = index.ToString(),
                                TargetValue = actual.Value<string>(),
                                TargetPair = new KeyValuePair<string, JToken>(index.ToString(), actual)
                            });
                        }
                        else
                        {
                            diffs.Add(new ComparisonDifference()
                            {
                                DifferenceType = ComparisonDifferenceType.DifferentValue,
                                SourceKey = $"{arrayName}[{index}]",
                                SourceValue = expected.Value<string>(),
                                SourcePair = new KeyValuePair<string, JToken>($"{arrayName}[{index}]", expected),
                                TargetKey = $"{arrayName}[{index}]",
                                TargetValue = actual.Value<string>(),
                                TargetPair = new KeyValuePair<string, JToken>($"{arrayName}[{index}]", actual)
                            });
                        }
                    }
                }
            }
            return diffs;
        }
    }
}
