﻿using MarcinWork.Core.Tools.JsonComparer.Models;
using MarcinWork.Core.Tools.JsonComparer.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace MarcinWork.Core.Tools.JsonComparer
{
    public class CompareEngine<Source, Target>
    {
        private JObject _source;
        private JObject _target;
        private ComparisonSettings _settings;

        public CompareEngine(Source source, Target target, ComparisonSettings settings)
        {
            _settings = settings;
            _source = BuildJobject(source);
            _target = BuildJobject(target);
        }

        private JObject BuildJobject<T>(T obj)
        {
            if (typeof(T) == typeof(JObject))
            {
                return obj as JObject;
            }

            if (typeof(T) == typeof(string))
            {
                return JObject.Parse(obj.ToString());
            }
            
            return JObjectFromObject(obj);
        }

        private JObject JObjectFromObject(object obj)
        {
            var jsonString =
                JsonConvert.SerializeObject(
                        obj,
                        Formatting.Indented,
                        _settings.SerializerSettings
                    );

            var json = JObject.Parse(jsonString);
            return json;
        }

        public ComparisonResult Compare()
        {
            return ComparisonService.CompareObjects(_source, _target);
        }
    }
}
